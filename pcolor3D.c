#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "math.h"
#include <pthread.h>
#include <time.h>
#include <string.h>
// Compute a pseudorandom integer.
// Output value in range [0, 32767]
inline int fast_rand(int *seed) {
    *seed = (214013* (*seed)+2531011);
    return (*seed>>16)&0x7FFF;
}


#define NUM_CORES 16


struct edge;
struct Element;

typedef struct element
{
    int type;
    int nodes[4];
    int pos;
    struct edge *sides[4];
} Element;


typedef struct edge
{
    pthread_mutex_t *locks[7];
    struct edge *adj[6];
    int visited;
    int color;
    struct element *le, *re;


    pthread_mutex_t lock;
    struct edge *lock_list[7];

    int nodes[3];
    int pos;
} Edge;

typedef struct Node
{
    double coords[3];
} Node;

typedef struct Arglist
{
    int idx;
    Element **tet_list;
    Edge **pconflict_list;
    Edge **edge_list;
    int pconflict_size;
    int *running;
} Arglist;

typedef struct Arglist_adj
{
    Edge **edge_list;
    int count;
} Arglist_adj;




#define SWAP(x,y) if (d[y]->pos < d[x]->pos) { Edge *tmp = d[x]; d[x] = d[y]; d[y] = tmp; }
static void sort3(Edge *d[3]){
    SWAP(1, 2);
    SWAP(0, 2);
    SWAP(0, 1);
}
static void sort4(Edge *d[4]){
    SWAP(0, 1);
    SWAP(2, 3);
    SWAP(0, 2);
    SWAP(1, 3);
    SWAP(1, 2);
}
static void sort6(Edge *d[6]){
    SWAP(1, 2);
    SWAP(0, 2);
    SWAP(0, 1);
    SWAP(4, 5);
    SWAP(3, 5);
    SWAP(3, 4);
    SWAP(0, 3);
    SWAP(1, 4);
    SWAP(2, 5);
    SWAP(2, 4);
    SWAP(1, 3);
    SWAP(2, 3);
}
static void sort7(Edge *d[7]){
SWAP(1, 2);
SWAP(0, 2);
SWAP(0, 1);
SWAP(3, 4);
SWAP(5, 6);
SWAP(3, 5);
SWAP(4, 6);
SWAP(4, 5);
SWAP(0, 4);
SWAP(0, 3);
SWAP(1, 5);
SWAP(2, 6);
SWAP(2, 5);
SWAP(1, 3);
SWAP(2, 4);
SWAP(2, 3);
}
#undef SWAP

#define SWAP 0
#define RESOLVE 1
#define NO_DOUBLE 2
#define DOUBLE 3



int verbose;


int indexof(Element *t, Element **list, int size){
    for(int i = 0; i < size; i++){
        if( t == list[i])
            return i;
    }
    return -1;
}

void add(Element *t, Element **list, int *size){
    list[*size] = t;
    *size = *size + 1 ;
}






inline void swap(int *a, int *b){
    int temp;
    temp = *b;
    *b = *a;
    *a = temp;
}



inline void populate_hash(int *color_hash, int *colors){
    color_hash[0] = 0; color_hash[1] = 0; color_hash[2] = 0; color_hash[3] = 0; color_hash[4] = 0;
    color_hash[colors[0]]++;
    color_hash[colors[1]]++;
    color_hash[colors[2]]++;
    if(colors[3] > -1){
        color_hash[colors[3]]++;
        color_hash[colors[4]]++;
        color_hash[colors[5]]++;
    }
}

void find(Edge *p_edge, int *d1, int* d2, int *d3, int* d4, int *colors){
    *d1 = -1; *d2 = -1; *d3 = -1; *d4 = -1;
    for(int i = 0; i < 6; i++){
        for(int j = i+1; j < 6; j++){
            if( (colors[i] == colors[j]) && (colors[i]!=0)  ){
                if(*d1 == -1){
                    *d1 = i; *d2 = j;
                }
                else{
                    *d3 = i; *d4 = j;
                    break;
                }
            }
        }
    }
    
}

inline int resolve(Edge *p_edge, int color_hash[5]){
    int pos = 1;
    while(color_hash[pos] > 0 && pos < 5)
        pos++;
    
    if(1 <= pos && pos <=4)
        return pos;
    else
        return 0;
}

int swap_with(Edge *p_edge, int conflict_ID, 
                int d1, int d2, int d3, int d4, 
                int *seed, 
                int *colors, 
                int visited[6]){
    int swap_list[] = {1,1,1,0,0,0};
    int swap_ID[] = {-1,-1,-1,-1,-1,-1};
    int swap_count = 0;
    int r;
    
    if(colors[3]>-1){
        swap_list[3] = 1;swap_list[4] = 1;swap_list[5] = 1;
    }
    
    if(d1 > -1 && d2 > -1){
        swap_list[d1] = 0;swap_list[d2] = 0;
    }
    if(d3 > -1 && d4 > -1){
        swap_list[d3] = 0;swap_list[d4] = 0;
    }
    
    // for(int i = 0; i < 6; i++){
    //     if(swap_list[i] && visited[i] != conflict_ID && colors[i] != 0){
    //         swap_ID[swap_count] = i;
    //         swap_count++;
    //     }
    // }

    if(swap_list[0] && visited[0] != conflict_ID && colors[0] != 0){
        swap_ID[swap_count] = 0;
        swap_count++;
    }
    if(swap_list[1] && visited[1] != conflict_ID && colors[1] != 0){
        swap_ID[swap_count] = 1;
        swap_count++;
    }
    if(swap_list[2] && visited[2] != conflict_ID && colors[2] != 0){
        swap_ID[swap_count] = 2;
        swap_count++;
    }
    if(swap_list[3] && visited[3] != conflict_ID && colors[3] != 0){
        swap_ID[swap_count] = 3;
        swap_count++;
    }
    if(swap_list[4] && visited[4] != conflict_ID && colors[4] != 0){
        swap_ID[swap_count] = 4;
        swap_count++;
    }
    if(swap_list[5] && visited[5] != conflict_ID && colors[5] != 0){
        swap_ID[swap_count] = 5;
        swap_count++;
    }


    // (x-(x/y)*y)
    if(swap_count>0){
        r = fast_rand(seed);
        return swap_ID[r - (r / swap_count) * swap_count];
        // return swap_ID[fast_rand(seed)%swap_count];
    }
    else
        return -1;
}


void *padj(void *in_padj_arg){
    Arglist_adj *args = (Arglist_adj *) in_padj_arg;
    int count = args->count;

    Edge **edge_list = args->edge_list;
    // build adjacency lists
    Edge *p_edge;
    Element *le, *re;
    for(int e = 0; e < count; e++){
        p_edge = edge_list[e];
        le = p_edge->le;
        re = p_edge->re;


        pthread_mutex_init ( &(p_edge->lock), NULL);


         p_edge->adj[0] = le->sides[0] == p_edge ? le->sides[3]: le->sides[0];
         p_edge->adj[1] = le->sides[1] == p_edge ? le->sides[3]: le->sides[1];
         p_edge->adj[2] = le->sides[2] == p_edge ? le->sides[3]: le->sides[2];

         p_edge->lock_list[0] = p_edge->adj[0];
         p_edge->lock_list[1] = p_edge->adj[1];
         p_edge->lock_list[2] = p_edge->adj[2];

         if(re){
             p_edge->adj[3] = re->sides[0] == p_edge ? re->sides[3]: re->sides[0];
             p_edge->adj[4] = re->sides[1] == p_edge ? re->sides[3]: re->sides[1];
             p_edge->adj[5] = re->sides[2] == p_edge ? re->sides[3]: re->sides[2];

             p_edge->lock_list[3] = p_edge->adj[3];
             p_edge->lock_list[4] = p_edge->adj[4];
             p_edge->lock_list[5] = p_edge->adj[5];

             p_edge->lock_list[6] = p_edge;
             sort7(p_edge->lock_list);
             for(int pp = 0; pp < 7; pp++)
                p_edge->locks[pp] = &p_edge->lock_list[pp]->lock;

         }
         else{
             p_edge->adj[3] = NULL;
             p_edge->adj[4] = NULL;
             p_edge->adj[5] = NULL;

             p_edge->lock_list[3] = p_edge;
             p_edge->lock_list[4] = NULL;
             p_edge->lock_list[5] = NULL;
             p_edge->lock_list[6] = NULL;

             sort4(p_edge->lock_list);
             for(int pp = 0; pp < 4; pp++)
                p_edge->locks[pp] = &p_edge->lock_list[pp]->lock;
         
             p_edge->locks[4] = NULL;
             p_edge->locks[5] = NULL;
             p_edge->locks[6] = NULL;
         }


    }

    return NULL;
}



void *pcolor(void *in_arg)
{   
    struct timespec pstart, pend;
    clock_gettime(CLOCK_THREAD_CPUTIME_ID, &pstart);

    int seed = time(NULL);
    Arglist *args = (Arglist *) in_arg;
    int conflict_size = args->pconflict_size;
    int cs_0 = conflict_size;

    if(verbose)
        printf("\n Thread %i has started with %i conflicts\n", args->idx, conflict_size);

    Edge **conflict_list = args->pconflict_list;
    Edge *curr_conflict;
    int r_color, d1, d2, d3, d4;
    int color_hash[] = {0,0,0,0,0};
    int colors[] = {-1,-1,-1,-1,-1,-1};
        Edge *swap, *adj[6];
    int visited[6];
    int conflict_ID = 0;
    int iteration = 0, swap_count = 0, double_count = 0, resolve_count = 0;

    int ACTION;
    pthread_mutex_t *local_locks[7];
    // conflict resolution
    int *running = args->running;
    int cont = 1;

    int swap_idx;
    while(conflict_size > 0 && (cont || conflict_size < 1e3)){
        curr_conflict = conflict_list[conflict_size-1];
        local_locks[0] =  curr_conflict->locks[0];
        local_locks[1] =  curr_conflict->locks[1];
        local_locks[2] =  curr_conflict->locks[2];
        local_locks[3] =  curr_conflict->locks[3];
        local_locks[4] =  curr_conflict->locks[4];
        local_locks[5] =  curr_conflict->locks[5];
        local_locks[6] =  curr_conflict->locks[6];

        pthread_mutex_lock(local_locks[0]);
        pthread_mutex_lock(local_locks[1]);
        pthread_mutex_lock(local_locks[2]);
        pthread_mutex_lock(local_locks[3]);
        if(local_locks[6]){
            pthread_mutex_lock(local_locks[4]);
            pthread_mutex_lock(local_locks[5]);
            pthread_mutex_lock(local_locks[6]);
        }

        adj[0] = curr_conflict->adj[0]; 
        adj[1] = curr_conflict->adj[1]; 
        adj[2] = curr_conflict->adj[2]; 
        adj[3] = curr_conflict->adj[3]; 
        adj[4] = curr_conflict->adj[4]; 
        adj[5] = curr_conflict->adj[5]; 

        curr_conflict->visited = conflict_ID;
        colors[0] = adj[0]->color; visited[0] = adj[0]->visited;
        colors[1] = adj[1]->color; visited[1] = adj[1]->visited;
        colors[2] = adj[2]->color; visited[2] = adj[2]->visited;

        if(curr_conflict->adj[3]){
            colors[3] = adj[3]->color;visited[3] = adj[3]->visited;
            colors[4] = adj[4]->color;visited[4] = adj[4]->visited;
            colors[5] = adj[5]->color;visited[5] = adj[5]->visited;
        }
        else{
            colors[3] = -1;
            colors[4] = -1;
            colors[5] = -1;
        }


        color_hash[0] = 0; color_hash[1] = 0; color_hash[2] = 0; color_hash[3] = 0; color_hash[4] = 0;
        color_hash[colors[0]]++;
        color_hash[colors[1]]++;
        color_hash[colors[2]]++;
        if(colors[3] > -1){
            color_hash[colors[3]]++;
            color_hash[colors[4]]++;
            color_hash[colors[5]]++;
        }



        // GET ACTION
        r_color = resolve(curr_conflict, color_hash);
        if(r_color){
            ACTION = RESOLVE;
        }
        else{
            find(curr_conflict, &d1, &d2, &d3, &d4, colors);
            swap_idx = swap_with(curr_conflict, conflict_ID, d1, d2, d3, d4, &seed, colors,visited);
        
            if(swap_idx>-1)
                ACTION = SWAP;
            else if(d1 < 0 || d2 < 0 || d3 < 0 || d4 < 0){
                ACTION = NO_DOUBLE;
            }
            else{
                ACTION = DOUBLE;
            }
        }

        switch(ACTION){
            case RESOLVE:
                curr_conflict->color = r_color;
                conflict_size--;
                conflict_ID = fast_rand(&seed);
                resolve_count ++;
            break;
            case SWAP:
                swap = adj[swap_idx];
                curr_conflict->color = swap->color;
                swap->color = 0;
                conflict_list[conflict_size-1] = swap;
                swap_count++;
            break;
            case DOUBLE:
                curr_conflict->color= adj[d1]->color;
                adj[d1]->color = 0;
                adj[d2]->color = 0;
                conflict_list[conflict_size-1] = adj[d1];
                conflict_list[conflict_size  ] = adj[d2];
                conflict_size++;
                conflict_ID = fast_rand(&seed);
                double_count++;
            break;
            case NO_DOUBLE:
                conflict_ID = fast_rand(&seed);
            break;
            default:
                printf("Invalid.\n" );
        }

        iteration++;

        pthread_mutex_unlock(local_locks[0]);
        pthread_mutex_unlock(local_locks[1]);
        pthread_mutex_unlock(local_locks[2]);
        pthread_mutex_unlock(local_locks[3]);
        if(local_locks[6]){
            pthread_mutex_unlock(local_locks[4]);
            pthread_mutex_unlock(local_locks[5]);
            pthread_mutex_unlock(local_locks[6]);
        }

        for(int t = 0; t < NUM_CORES; t++)
            cont = cont && running[t];
    }



    clock_gettime(CLOCK_THREAD_CPUTIME_ID, &pend);
    // clock_gettime(CLOCK_MONOTONIC, &end);
    double elapsed;

    elapsed = pend.tv_sec-pstart.tv_sec;
    elapsed+= (pend.tv_nsec-pstart.tv_nsec)/1.e9;

    args->pconflict_size = conflict_size;

    running[args->idx] = 0;

    if(verbose)
        printf("Thread %i has done (%i) resolves, (%i) swaps, (%i) doubles in %lf seconds\n", args->idx, resolve_count, swap_count, double_count, elapsed);
    

return NULL;
}


int main(int argc, char * argv[]) {

    // printf("This system has %d cores available.\n\n",countCores());


    int node_count=0, tet_count=0, edge_count=0, size;
    int err;
    char line[100];

    double elapsed;

    if(argc == 4 && strcmp(argv[3] , "v")==0)
        verbose = 1;
    else
        verbose = 0;

    struct timespec pstart, pend;
    clock_gettime(CLOCK_MONOTONIC, &pstart);


    char in_node[40], in_ele[40], in_face[40], in_neigh[40], in_t2f[40], in_cmsh[40];
    strcpy(in_node, argv[1]);
    strcpy(in_ele, argv[1]);
    strcpy(in_face, argv[1]);
    strcpy(in_t2f, argv[1]);
    strcpy(in_cmsh, argv[1]);

    strcat(in_node, ".node");
    strcat(in_ele, ".ele");
    strcat(in_face, ".face");
    strcat(in_t2f, ".t2f");
    strcat(in_cmsh, ".cmsh");

    FILE * inFile = fopen(in_node, "r");
    err = fscanf(inFile, "%i %*i %*i", &node_count);
    Node *node_list = (Node *) malloc( node_count * sizeof(Node));
    for(int n = 0; n < node_count; n++){
        err = fscanf(inFile, "%*i %lf %lf %lf", node_list[n].coords + 0, node_list[n].coords + 1, node_list[n].coords + 2);
    }
    fclose(inFile);

    inFile = fopen(in_ele, "r");
    err = fscanf(inFile, "%i %*i %*i", &tet_count);
    Element **tet_list = (Element **) malloc( tet_count * sizeof(Element*));
    for(int t = 0; t < tet_count; t++){
        tet_list[t] = (Element *) malloc(sizeof(Element));
        err = fscanf(inFile, "%*i %i %i %i %i", tet_list[t]->nodes + 0, tet_list[t]->nodes + 1, tet_list[t]->nodes + 2, tet_list[t]->nodes + 3);
    }
    fclose(inFile);

    inFile = fopen(in_face, "r");
    err = fscanf(inFile, "%i %*i", &edge_count);
    Edge **edge_list = (Edge **) malloc( edge_count * sizeof(Edge*));
    int le, re;
    for(int t = 0; t < edge_count; t++){
        edge_list[t] = (Edge *) malloc(sizeof(Edge));
        err = fscanf(inFile, "%*i %i %i %i %*i %i %i", edge_list[t]->nodes + 0, edge_list[t]->nodes + 1, edge_list[t]->nodes + 2, &le, &re);

        if(le < re) // ensure ghost element is always the right element
            swap(&le, &re);
        edge_list[t]->le = tet_list[le];
        if(re > -1)
            edge_list[t]->re = tet_list[re];
        else
            edge_list[t]->re = NULL;

    }

    int f1, f2, f3, f4;
    inFile = fopen(in_t2f, "r");
    for(int t = 0; t < tet_count; t++){
        err = fscanf(inFile, "%*i %i %i %i %i", &f1, &f2, &f3, &f4);

        tet_list[t]->sides[0] = f1 > -1 ? edge_list[f1] : NULL;
        tet_list[t]->sides[1] = f2 > -1 ? edge_list[f2] : NULL;
        tet_list[t]->sides[2] = f3 > -1 ? edge_list[f3] : NULL;
        tet_list[t]->sides[3] = f4 > -1 ? edge_list[f4] : NULL;

    }
    fclose(inFile);


    clock_gettime(CLOCK_MONOTONIC, &pend);


    elapsed = pend.tv_sec-pstart.tv_sec;
    elapsed+= (pend.tv_nsec-pstart.tv_nsec)/1.e9;
    printf("Loading elapsed %lf\n", elapsed);

    
    printf("node_count is %i\n", node_count);
    printf("tetrahedron count is %i\n", tet_count);
    printf("edge_count %i\n", edge_count);




    
    for (int i = 0; i < tet_count; i++)
        tet_list[i]->pos = i;
    for (int i = 0; i < edge_count; i++)
        edge_list[i]->pos = i;
    


    // build adjacency lists
    clock_gettime(CLOCK_MONOTONIC, &pstart);

    pthread_t tid_adj[NUM_CORES];
    Arglist_adj in_args_adj[NUM_CORES];
    int count[NUM_CORES];
    int offset = 0;
    int error;

    for(int t = 0; t < NUM_CORES; t++){
        count[t] = edge_count / NUM_CORES;
        if(t < edge_count % NUM_CORES)
            count[t]++;

        in_args_adj[t].edge_list = edge_list + offset;
        in_args_adj[t].count = count[t];
        offset += count[t];
    }
    printf("\n");


    for(int t = 0; t < NUM_CORES; t++){
        error = pthread_create(&(tid_adj[t]), NULL, &padj, &in_args_adj[t]);
    }

    for (int i = 0; i < NUM_CORES; i++)
       pthread_join(tid_adj[i], NULL);

    clock_gettime(CLOCK_MONOTONIC, &pend);

    elapsed = pend.tv_sec-pstart.tv_sec;
    elapsed+= (pend.tv_nsec-pstart.tv_nsec)/1.e9;

    printf("\nAdjacency seconds elapsed %lf\n", elapsed);




    printf("Coloring!\n");
    clock_gettime(CLOCK_MONOTONIC, &pstart);

    // initial greedy coloring

    int colors[] = {-1,-1,-1,-1,-1,-1};
    Edge **conflict_list = (Edge **) malloc(edge_count * sizeof(Edge*));

    // initial greedy coloring
    int color_hash[] = {0,0,0,0,0};
    int pos;
    int conflict_size = 0;
    Edge *p_edge;
    for(int e = 0; e < edge_count; e++){
        conflict_list[conflict_size] = NULL;
        p_edge = edge_list[e];
        p_edge->visited = -1;
        p_edge->color=0;
        conflict_list[conflict_size] = p_edge;
        conflict_size++;
    }


    pthread_t tid[NUM_CORES];
    Arglist in_args[NUM_CORES];

    Edge **pconflict_list[NUM_CORES];
    int pconflict_size[NUM_CORES];


    int running[NUM_CORES];

    printf("Each thread will resolve ~%i conflicts\n", conflict_size/NUM_CORES);
    
    offset = 0;
    for(int t = 0; t < NUM_CORES; t++){
        pconflict_size[t] = conflict_size / NUM_CORES;
        if(t < conflict_size % NUM_CORES)
            pconflict_size[t]++;

        if(verbose)
            printf("%i ", pconflict_size[t]);
        
        pconflict_list[t] = (Edge **) malloc(edge_count * sizeof(Edge *));
        memcpy(pconflict_list[t], conflict_list + offset, pconflict_size[t] * sizeof(Edge *));
        offset += pconflict_size[t];
    }
    printf("\n");



    while(conflict_size > 0){

        for(int t = 0; t < NUM_CORES; t++)
            running[t] = 1;

        for(int t = 0; t < NUM_CORES; t++){
            in_args[t].idx = t;
            in_args[t].tet_list = tet_list;
            in_args[t].edge_list = edge_list;
            in_args[t].pconflict_list = pconflict_list[t];
            in_args[t].pconflict_size = pconflict_size[t];
            in_args[t].running = running;
            error = pthread_create(&(tid[t]), NULL, &pcolor, &in_args[t]);
        }

        for (int i = 0; i < NUM_CORES; i++)
           pthread_join(tid[i], NULL);

       // rebalance
        conflict_size = 0;
        for(int t = 0; t < NUM_CORES; t++){
            pconflict_size[t] = in_args[t].pconflict_size;
            memcpy(conflict_list + conflict_size, pconflict_list[t],  pconflict_size[t] * sizeof(Edge *));
            conflict_size+=in_args[t].pconflict_size;
        }




        offset = 0 ;
        for(int t = 0; t < NUM_CORES; t++){
            pconflict_size[t] = conflict_size / NUM_CORES;
            if(t < conflict_size % NUM_CORES)
                pconflict_size[t]++;

            if(verbose)
                printf("%i ", pconflict_size[t]);
            memcpy(pconflict_list[t], conflict_list + offset, pconflict_size[t] * sizeof(Edge *));
            offset += pconflict_size[t];

        }




    }

    printf("\nDone coloring. %i\n", conflict_size);
    // end = clock();
    clock_gettime(CLOCK_MONOTONIC, &pend);
    // clock_gettime(CLOCK_MONOTONIC, &end);

    elapsed = pend.tv_sec-pstart.tv_sec;
    elapsed+= (pend.tv_nsec-pstart.tv_nsec)/1.e9;

    printf("\nseconds elapsed %lf\n", elapsed);


    color_hash[0] = 0; color_hash[1] = 0; color_hash[2] = 0; color_hash[3] = 0; color_hash[4] = 0;
    for(int e = 0; e < edge_count; e++){
        color_hash[edge_list[e]->color]++;
    }
    for(int i = 0; i < 5; i++)
        printf("color %i: %i\n", i, color_hash[i]);
    
    for(int elem=0; elem < tet_count; elem++){
        color_hash[0] = 0; color_hash[1] = 0; color_hash[2] = 0; color_hash[3] = 0; color_hash[4] = 0;
        color_hash[tet_list[elem]->sides[0]->color]++;
        color_hash[tet_list[elem]->sides[1]->color]++;
        color_hash[tet_list[elem]->sides[2]->color]++;
        color_hash[tet_list[elem]->sides[3]->color]++;
        if(color_hash[1] != 1 || color_hash[2] != 1 || color_hash[3] != 1 || color_hash[4] != 1 ){
            printf("problem %i %i %i %i %i\n", color_hash[0], color_hash[1], color_hash[2], color_hash[3], color_hash[4]);
            printf("problem s1 %i s2 %i s3 %i s4 %i\n", tet_list[elem]->sides[0]->color, tet_list[elem]->sides[1]->color, tet_list[elem]->sides[2]->color, tet_list[elem]->sides[2]->color);
            exit(-1);
        }
    }

    int index=0;
    for(int c = 1; c <= 4; c++){
        for(int k = 0; k < edge_count; k++){
            if(edge_list[k]->color == c){
                edge_list[k]->pos = index;
                index++;
            }
        }
    }

    Edge **edge_list_ordered = (Edge**) malloc(edge_count * sizeof(Edge *));
    for(int k = 0; k < edge_count; k++)
        edge_list_ordered[edge_list[k]->pos] = edge_list[k];

    free(edge_list);
    edge_list = edge_list_ordered; 





    printf("writing cmsh file %s\n", in_cmsh);
    FILE *outFile = fopen(in_cmsh, "w");
    color_hash[0] = 0; color_hash[1] = 0; color_hash[2] = 0; color_hash[3] = 0; color_hash[4] = 0;
    for(int e = 0; e < edge_count; e++){
        color_hash[edge_list[e]->color]++;
    }
   fprintf(outFile, "%i\n", node_count);
   for (int i = 0; i < node_count; i++)
       fprintf(outFile, "%.015lf %.015lf %.015lf\n",node_list[i].coords[0]
                                                   ,node_list[i].coords[1]
                                                   ,node_list[i].coords[2]);
   fprintf(outFile, "%i\n", tet_count);
   for (int i = 0; i < tet_count; i++)
       fprintf(outFile, "%i %i %i %i %i %i %i %i\n",tet_list[i]->nodes[0],
                                                    tet_list[i]->nodes[1],
                                                    tet_list[i]->nodes[2],
                                                    tet_list[i]->nodes[3],
                                                    tet_list[i]->sides[0]->pos, 
                                                    tet_list[i]->sides[1]->pos, 
                                                    tet_list[i]->sides[2]->pos, 
                                                    tet_list[i]->sides[3]->pos);

   fprintf(outFile, "%i\n", edge_count);
   for (int i = 0; i < edge_count; i++){
    if( edge_list[i]->re)
        fprintf(outFile, "%i %i %i %i %i %i\n",
                                             edge_list[i]->nodes[0],
                                             edge_list[i]->nodes[1],
                                             edge_list[i]->nodes[2],
                                             edge_list[i]->le->pos, 
                                             edge_list[i]->re->pos, 
                                             edge_list[i]->color -1);
    else
        fprintf(outFile, "%i %i %i %i %i %i\n",
                                             edge_list[i]->nodes[0],
                                             edge_list[i]->nodes[1],
                                             edge_list[i]->nodes[2],
                                             edge_list[i]->le->pos, 
                                             -1, 
                                             edge_list[i]->color -1);
    }
    fclose(outFile);

    
}
