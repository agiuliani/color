#include <stdio.h>
#include <time.h>
#include "math.h"
#include "uthash.h"

struct edge;
struct Element;

typedef struct element
{
    int type;
    int nodes[4];
    int pos;
    int concave;
    struct edge *sides[4];
    struct element *neighbors[4];
    struct element *match;
    int visited;
} Element;


typedef struct edge
{
    char name[100];
    int nodes[2];
    int pos;
    int boundary;
    int color;
    struct element *le, *re;
    struct edge *adj[4];
    int visited;
    UT_hash_handle hh;
} Edge;

typedef struct Node
{   
    int boundary;
    double coords[2];
} Node;



void get_id(char *id, int n1, int n2){
        sprintf(id, "%i %i", n1, n2);
    }






Edge *edge_hash = NULL;
Edge *q_edge_hash = NULL;

Edge* edge_insert(int n1, int n2, Element *in_tri, Edge **edge_list, int *edge_count){
    char id1[100];
    char id2[100];
    get_id(id1, n1 , n2 );
    get_id(id2, n2 , n1 );
    Edge *exists,*exists1;
    HASH_FIND_STR( edge_hash, id1, exists);
    HASH_FIND_STR( edge_hash, id2, exists1);

    if(!exists)
        exists = exists1;

    if(!exists){
        exists = (Edge *) malloc(sizeof(Edge));
        sprintf(exists->name, "%i %i", n1, n2);
        exists->nodes[0] = n1;
        exists->nodes[1] = n2;
        exists->le = in_tri;
        exists->re = NULL;
        exists->boundary = 0;
        exists->color = 0;
        edge_list[*edge_count] = exists;
        HASH_ADD_STR( edge_hash, name,  exists);
        *edge_count = *edge_count + 1;
    }
    else{
        if(exists->boundary)
            exists->le = in_tri;
        else
            exists->re = in_tri;
    }

    return exists;
}

Edge* q_edge_insert(int n1, int n2, Element *in_tri, Edge **edge_list, int *edge_count){
    char id1[100];
    char id2[100];
    get_id(id1, n1 , n2 );
    get_id(id2, n2 , n1 );
    Edge *exists,*exists1;
    HASH_FIND_STR(q_edge_hash, id1, exists);
    HASH_FIND_STR(q_edge_hash, id2, exists1);

    if(!exists)
        exists = exists1;

    if(!exists){
        exists = (Edge *) malloc(sizeof(Edge));
        sprintf(exists->name, "%i %i", n1, n2);
        exists->nodes[0] = n1;
        exists->nodes[1] = n2;
        exists->le = in_tri;
        exists->re = NULL;
        exists->boundary = 0;
        exists->color = 0;
        edge_list[*edge_count] = exists;
        HASH_ADD_STR( q_edge_hash, name,  exists);
        *edge_count = *edge_count + 1;
    }
    else{
        if(exists->boundary)
            exists->le = in_tri;
        else
            exists->re = in_tri;
    }

    return exists;
}





int main(int argc, char * argv[]) {
    time_t t;
    srand((unsigned) time(&t));
    int node_count=0, triangle_count=0, boundary_count=0, edge_count=0, size;
    char line[100];
    FILE * inFileP = fopen(argv[1], "r");
    fgets(line, sizeof line, inFileP);
    while(strncmp(line,"$Nodes", 6)!=0)
        fgets(line, sizeof line, inFileP);
    fscanf(inFileP, "%i", &node_count);

    printf("node_count is %i\n", node_count);
    Node *node_list = (Node *) malloc( node_count * sizeof(Node));


    for(int n = 0; n < node_count; n++){
        fscanf(inFileP, "%*i %lf %lf %*lf", node_list[n].coords + 0, node_list[n].coords + 1);
        node_list[n].boundary = 0;
    }

    fgets(line, sizeof line, inFileP);
    while(strncmp(line,"$Elements", 8)!=0)
        fgets(line, sizeof line, inFileP);
    fgets(line, sizeof line, inFileP) ;
    sscanf(line, "%i", &size);


    Edge **boundary_list = (Edge **) malloc( size * sizeof(Edge*));
    Edge **edge_list = (Edge **) malloc( 3*size * sizeof(Edge*));
    Element **triangle_list = (Element **) malloc( size * sizeof(Element*));
    int type;

    Edge *temp;
    Edge *exists, *exists1;




    for(int n = 0; n < size; n++){
        type = -1;
        fgets(line, sizeof line, inFileP) ;
        sscanf(line, "%*i %i", &type);
        if(type == 1){

            temp = (Edge *) malloc(sizeof(Edge));
            sscanf(line, "%*i %*i %*i %*i %*i %i %i", temp->nodes + 0, temp->nodes + 1);
            temp->nodes[0]--;
            temp->nodes[1]--;
            temp->le = NULL;
            temp->re = NULL;
            temp->boundary = 1;
            temp->color = 0;

            node_list[temp->nodes[0]].boundary = 1;
            node_list[temp->nodes[1]].boundary = 1;

            sprintf(temp->name, "%i %i", temp->nodes[0], temp->nodes[1]);
            HASH_FIND_STR( edge_hash, temp->name, exists);
            sprintf(temp->name, "%i %i", temp->nodes[1], temp->nodes[0]);
            HASH_FIND_STR( edge_hash, temp->name, exists1);

            exists = !exists ? exists1 : exists;
            if(!exists){
                HASH_ADD_STR( edge_hash, name,  temp);
                boundary_list[boundary_count] = temp;
                edge_list[edge_count] = temp;
                edge_count++;
                boundary_count++;
            }
            else
                free(temp);
        }
        else if(type == 2){
            triangle_list[triangle_count] = (Element *) malloc(sizeof(Element));
            sscanf(line, "%*i %*i %*i %*i %*i %i %i %i", triangle_list[triangle_count]->nodes + 0, triangle_list[triangle_count]->nodes + 1, triangle_list[triangle_count]->nodes + 2);
            triangle_list[triangle_count]->type = 2;
            triangle_list[triangle_count]->nodes[0]--;
            triangle_list[triangle_count]->nodes[1]--;
            triangle_list[triangle_count]->nodes[2]--;
            triangle_count++;
        }
    }

    int n1, n2;
    //initialize faces
    for(int tri = 0; tri < triangle_count; tri++){
        // add side 0
        n1 = triangle_list[tri]->nodes[0];
        n2 = triangle_list[tri]->nodes[1];
        triangle_list[tri]->sides[0] = edge_insert(n1, n2, triangle_list[tri], edge_list, &edge_count);


        // add side 1
        n1 = triangle_list[tri]->nodes[1];
        n2 = triangle_list[tri]->nodes[2];
        triangle_list[tri]->sides[1] = edge_insert(n1, n2, triangle_list[tri], edge_list, &edge_count);


        // add side 2
        n1 = triangle_list[tri]->nodes[2];
        n2 = triangle_list[tri]->nodes[0];
        triangle_list[tri]->sides[2] = edge_insert(n1, n2, triangle_list[tri], edge_list, &edge_count);
    }

    // for(int tri = 0; tri < triangle_count; tri++){
    //     triangle_list[tri]->pos = tri;
    // }
    // for(int e = 0; e < edge_count; e++){
    //     edge_list[e]->pos = e;
    // }
    printf("element count %i\n", size);
    printf("boundary_count %i\n", boundary_count);
    printf("triangle_count %i\n", triangle_count);
    printf("edge_count %i\n", edge_count);
    printf("Found all the faces!\n");
    //found all the faces

    // build adjacency lists
    Edge *p_edge;
    Element *le, *re;
    for(int e = 0; e < edge_count; e++){
        p_edge = edge_list[e];
        le = p_edge->le;
        re = p_edge->re;

        p_edge->adj[0] = le->sides[0] == p_edge ? le->sides[2]: le->sides[0];
        p_edge->adj[1] = le->sides[1] == p_edge ? le->sides[2]: le->sides[1];

        if(re){
            p_edge->adj[2] = re->sides[0] == p_edge ? re->sides[2]: re->sides[0];
            p_edge->adj[3] = re->sides[1] == p_edge ? re->sides[2]: re->sides[1];
        }
    }


    // initialize neighbors.
    Element *curr_elem;
    Edge *curr_edge;
    for(int i=0; i < triangle_count; i++){
        curr_elem = triangle_list[i];
        for(int s = 0; s < 3; s++) {
            curr_edge = curr_elem->sides[s];
            curr_elem->neighbors[s] = (curr_edge->re == curr_elem) ? curr_edge->le : curr_edge->re;
        }
    }


    
    for (int i = 0; i < triangle_count; i++)
        triangle_list[i]->pos = i;
    for (int i = 0; i < edge_count; i++)
        edge_list[i]->pos = i;

    double V1x,V1y,V2x,V2y,V3x,V3y,J;
    int temp_int;

    FILE * outFileP = fopen("out.pmsh", "w");
    fprintf(outFileP, "%i\n", node_count);

    for (int i = 0; i < node_count; i++)
           fprintf(outFileP, "%lf %lf\n",  node_list[i].coords[0], node_list[i].coords[1]);
    fprintf(outFileP, "%i\n", triangle_count);
    for (int i = 0; i < triangle_count; i++){
		V1x = node_list[triangle_list[i]->nodes[0]].coords[0];
		V1y = node_list[triangle_list[i]->nodes[0]].coords[1];
		V2x = node_list[triangle_list[i]->nodes[1]].coords[0];
		V2y = node_list[triangle_list[i]->nodes[1]].coords[1];
		V3x = node_list[triangle_list[i]->nodes[2]].coords[0];
		V3y = node_list[triangle_list[i]->nodes[2]].coords[1];

		// enforce strictly positive jacobian
		J = (V2x - V1x) * (V3y - V1y) - (V3x - V1x) * (V2y - V1y);
		// swap vertex 0 and 1
		if(J < 0){
			temp_int = triangle_list[i]->nodes[0];
			triangle_list[i]->nodes[0] = triangle_list[i]->nodes[1];
			triangle_list[i]->nodes[1] = temp_int;
		}
       	fprintf(outFileP, "%i %i %i %i %i %i\n", triangle_list[i]->nodes[0], triangle_list[i]->nodes[1], triangle_list[i]->nodes[2], triangle_list[i]->sides[0]->pos, triangle_list[i]->sides[1]->pos, triangle_list[i]->sides[2]->pos);
    }
    fprintf(outFileP, "%i\n", edge_count);
    for (int i = 0; i < edge_count; i++){
        if(edge_list[i]->re)
           fprintf(outFileP, "%i %i %i %i\n", edge_list[i]->nodes[0], edge_list[i]->nodes[1], edge_list[i]->le->pos, edge_list[i]->re->pos);
       else
           fprintf(outFileP, "%i %i %i %i\n", edge_list[i]->nodes[0], edge_list[i]->nodes[1], edge_list[i]->le->pos, -1);

    }



}