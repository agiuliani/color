#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "math.h"


int get_next(int curr, int **neigh, int *visited, int id){
    int n;

    n = neigh[curr][0];
    if(n>-1 && visited[n] != id)
        return n;

    n = neigh[curr][1];
    if(n>-1 && visited[n]  != id)
        return n;

    n = neigh[curr][2];
    if(n>-1 && visited[n] != id)
        return n;

    return -1;
}

void find(int p_edge, 
          int *left_elem, int *right_elem,
          int **adj,
          int *color,
         int *d1, int* d2){
    int color_list[] = {0,0,0,0};
    int le = left_elem[p_edge];
    int re = right_elem[p_edge];
    color_list[0] =color[adj[0][p_edge]];
    color_list[1] =color[adj[1][p_edge]];
    if(re > -1){
        color_list[2]= color[adj[2][p_edge]];
        color_list[3]= color[adj[3][p_edge]];
    }

    *d1 = -1; *d2 = -1;

    for(int i = 0; i < 4; i++){
        for(int j = i; j < 4; j++){
            if( (color_list[i] == color_list[j]) && (color_list[i]!=0) &&(color_list[j]!=0) && (i!=j)){
                *d1 = i; *d2 = j;
                break;
            }
        }
    }

}


int swap_with(int p_edge, int le, int re,
                int **adj ,
                int *color, int *visited,
                int conflict_ID, int d1, int d2){
    int swap_list[] = {1,1,0,0};
    int swap_ID[] = {-1,-1,-1,-1};
    int swap_count = 0;
    int pos;
    int choice ;
    
    if(re > -1){
        swap_list[2] = 1;
        swap_list[3] = 1;
    }

    if(d1 > -1 && d2 > -1){
        swap_list[d1] = 0;
        swap_list[d2] = 0;
    }   


   
    for(int i = 0; i < 4; i++){
        if(swap_list[i] && visited[adj[i][p_edge]] != conflict_ID && color[adj[i][p_edge]] != 0){
            swap_ID[swap_count] = i;
            swap_count++;
        }
    } 

    if(swap_count>0){
    	return adj[swap_ID[rand()%swap_count]][p_edge];
        // return p_edge->adj[swap_ID[rand()%swap_count]];
    }
    else
        return -1; 


}



int resolve(int *color_hash){
    int pos = 1;
    while(color_hash[pos] > 0)
        pos++;

    if(1 <= pos && pos <=3)
        return pos;
    else
        return 0;
}



int main(int argc, char * argv[]) {
    time_t t;
    srand((unsigned) time(&t));
    int node_count=0, tri_count=0 , edge_count = 0;
    char line[100];

    printf("Reading in from %s...\n", argv[1]);
    FILE * inFileP = fopen(argv[1], "r");


    printf("Getting node number...\n");
    fscanf(inFileP, "%i", &node_count);

    double *vertex_list = malloc(2 * node_count * sizeof(double));
    printf("Building the node list...\n");

    for (int i = 0; i < node_count; i++)
        fscanf(inFileP, "%lf %lf", vertex_list + 2*i, vertex_list + 2*i +1);


    printf("Getting element number...\n");
    fscanf(inFileP, "%i", &tri_count);

    int * v1 = malloc(tri_count * sizeof(int));
    int * v2 = malloc(tri_count * sizeof(int));
    int * v3 = malloc(tri_count * sizeof(int));

    int * s1 = malloc(tri_count * sizeof(int));
    int * s2 = malloc(tri_count * sizeof(int));
    int * s3 = malloc(tri_count * sizeof(int));


    printf("Building the element list...\n");
    for (int i = 0; i < tri_count; i++) 
        fscanf(inFileP, "%i %i %i %i %i %i", v1+i, v2+i, v3+i, s1+i, s2+i, s3+i);



    printf("Getting the face number...\n");
    fscanf(inFileP, "%i", &edge_count);

    int * face_v1 = malloc(sizeof(int) * edge_count);
    int * face_v2 = malloc(sizeof(int) * edge_count);

    int * left_elem = malloc(sizeof(int) * edge_count);
    int * right_elem = malloc(sizeof(int) * edge_count);

    printf("Building the face list...\n");
    for (int i = 0; i < edge_count; i++)
        fscanf(inFileP, "%i %i %i %i", face_v1 + i, face_v2 + i, left_elem + i, right_elem + i);

    fclose(inFileP);
    printf("There are %i elements, %i faces, and %i nodes\n", tri_count, edge_count, node_count);



    // build adjacency lists
    int le, re;
    int *adj[4] ;
    adj[0] = (int *) malloc(edge_count * sizeof(int));
    adj[1] = (int *) malloc(edge_count * sizeof(int));
    adj[2] = (int *) malloc(edge_count * sizeof(int));
    adj[3] = (int *) malloc(edge_count * sizeof(int));

    for(int e = 0; e < edge_count; e++){
        le = left_elem[e];
        re = right_elem[e];

        adj[0][e] = s1[le] == e ? s3[le]: s1[le];
        adj[1][e] = s2[le] == e ? s3[le]: s2[le];

        if(re > -1){
            adj[2][e] = s1[re] == e ? s3[re]: s1[re];
            adj[3][e] = s2[re] == e ? s3[re]: s2[re];
        }
        else{
            adj[2][e] = -1;
            adj[3][e] = -1;
        }
    }




    int *color = (int *) malloc(edge_count * sizeof(int));
    int *conflict_list = (int *) malloc(edge_count * sizeof(int));
    int *visited = (int *) malloc(edge_count * sizeof(int));
    clock_t begin , end;
    double color_timing, match_timing;

    for(int e = 0; e < edge_count ; e++)
        color[e] = 0;

    begin = clock();
    // initial greedy coloring
    int color_hash[] = {0,0,0,0};
    int pos;
    int conflict_size = 0;
    for(int e = 0; e < edge_count; e++){
        le = left_elem[e];
        re = right_elem[e];
        color_hash[0] = 0; color_hash[1] = 0; color_hash[2] = 0; color_hash[3] = 0;

        color_hash[color[adj[0][e]]]++;
        color_hash[color[adj[1][e]]]++;
        if(re > -1){
            color_hash[color[adj[2][e]]]++;
            color_hash[color[adj[3][e]]]++;
        }

        pos = -1;
        for(int i = 1; i <= 3; i++){
            if(color_hash[i] == 0 ){
                pos = i;
                break;
            }
        }

        if(1 <= pos && pos <=3)
            color[e] = pos;
        else{
            color[e]=0;
            conflict_list[conflict_size] = e;
            conflict_size++;
        }

        visited[e] = -1;
        visited[e] = -1;
    }



    printf("There are %i conflicts\n", conflict_size);
    int curr_conflict;

    int conflict_ID = 0;
    int iteration = 0;
    int r_color, d1, d2, swap;
    // conflict resolution
    while(conflict_size > 0){
        curr_conflict = conflict_list[conflict_size-1];
        visited[curr_conflict] = conflict_ID;

        // populate hash
        le = left_elem[curr_conflict];
        re = right_elem[curr_conflict];
        color_hash[0] = 0; color_hash[1] = 0; color_hash[2] = 0; color_hash[3] = 0;

        color_hash[color[adj[0][curr_conflict]]]++;
        color_hash[color[adj[1][curr_conflict]]]++;
        if(re > -1){
            color_hash[color[adj[2][curr_conflict]]]++;
            color_hash[color[adj[3][curr_conflict]]]++;
        }


        r_color = resolve(color_hash);
        if(r_color){ // can it be resolved?
            color[curr_conflict] = r_color;
            conflict_size--;
            conflict_ID++;
            continue;
        }

        find(curr_conflict, 
            left_elem, right_elem,
            adj ,
            color,
            &d1, &d2);
        swap = swap_with(curr_conflict, left_elem[curr_conflict], right_elem[curr_conflict],
                         adj,
                         color, visited, 
                         conflict_ID, d1, d2);
        if(swap > -1){ // swap
            color[curr_conflict] = color[swap];
            color[swap] = 0;
            conflict_list[conflict_size-1] = swap;
            continue;
        }


        if(d1 < 0 || d2 < 0){
            conflict_ID++;
            continue;
        }

        color[curr_conflict] = color[adj[d1][curr_conflict]];
        color[adj[d1][curr_conflict]] = 0;
        color[adj[d2][curr_conflict]] = 0;
        conflict_list[conflict_size-1] = adj[d1][curr_conflict]; 
        conflict_list[conflict_size] = adj[d2][curr_conflict]; 

        conflict_size++;
        conflict_ID++;



        if(iteration%100 ==0)
            printf("\r(%i) conflicts left.", conflict_size);
        iteration++;
    }

    printf("\nDone coloring. %i\n", conflict_size);

    end = clock();
    color_timing = (double)(end - begin) / CLOCKS_PER_SEC;
    printf("\nColoring took: %lf seconds \n\n", color_timing);

    int color_count[4];
    color_count[0] = 0;
    color_count[1] = 0;
    color_count[2] = 0;
    color_count[3] = 0;
    for(int i = 0; i < edge_count; i++){
        if(right_elem[i] == -1 )
            color_count[color[i]]++;
    }



    FILE * outFileP = fopen("out.cpmsh", "w");
    fprintf(outFileP, "%i\n", node_count);

    for (int i = 0; i < node_count; i++)
           fprintf(outFileP, "%lf %lf\n",  vertex_list[2*i], vertex_list[2*i +1]);
    fprintf(outFileP, "%i\n", tri_count);
    for (int i = 0; i < tri_count; i++){
           fprintf(outFileP, "%i %i %i %i %i %i\n", v1[i], v2[i], v3[i], s1[i], s2[i], s3[i]);
    }
    fprintf(outFileP, "%i\n", edge_count);
    for (int i = 0; i < edge_count; i++){
        if(right_elem[i] > -1)
           fprintf(outFileP, "%i %i %i %i %i\n", face_v1[i], face_v2[i], left_elem[i], right_elem[i], color[i]);
       else
           fprintf(outFileP, "%i %i %i %i %i\n", face_v1[i], face_v2[i], left_elem[i], -1, color[i]);

    }





}