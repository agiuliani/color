#include <stdio.h>
#include <time.h>
#include "math.h"
#include "uthash.h"
#include <pthread.h>

struct edge;
struct Element;

typedef struct element
{
    int type;
    int nodes[4];
    int pos;
    struct edge *sides[4];
    struct element *neighbors[4];

} Element;


typedef struct edge
{
    char name[100];
    int nodes[3];
    int pos;
    int boundary;
    int color;
    struct element *le, *re;
    struct edge *adj[6];

    int visited;
    UT_hash_handle hh;
} Edge;

typedef struct Node
{
    int boundary;
    double coords[3];
} Node;



void get_id(char *id, int n1, int n2, int n3){
    sprintf(id, "%i %i %i", n1, n2, n3);
}

int indexof(Element *t, Element **list, int size){
    for(int i = 0; i < size; i++){
        if( t == list[i])
            return i;
    }
    return -1;
}

void add(Element *t, Element **list, int *size){
    list[*size] = t;
    *size = *size + 1 ;
}




Edge *edge_hash = NULL;
Edge *q_edge_hash = NULL;


void swap(int *a, int *b){
    int temp;
    temp = *b;
    *b = *a;
    *a = temp;
}


Edge* edge_insert(int n1, int n2, int n3, int sn, Element *in_tet, Edge **edge_list, int *edge_count){
    char id[100];
    int a = n1, b = n2, c = n3;

    if (a < b) {
       if (c < a) swap(&a,&c);
    } else {
      if (b < c) swap(&a,&b);
      else swap(&a,&c);
    } 
    if(c<b) swap(&b,&c);

    get_id(id, a , b , c);

    Edge *exists = NULL;
    HASH_FIND_STR( edge_hash, id, exists);
    if(exists){
        if(exists->boundary){
            exists->le = in_tet;
        }
        else
            exists->re = in_tet;
    }
    if(!exists){
        exists = (Edge *) malloc(sizeof(Edge));
        sprintf(exists->name, "%i %i %i", a, b, c);
        exists->nodes[0] = n1;
        exists->nodes[1] = n2;
        exists->nodes[2] = n3;
        exists->le = in_tet;
        exists->re = NULL;
        exists->boundary = 0;
        exists->color = 0;
        edge_list[*edge_count] = exists;
        HASH_ADD_STR( edge_hash, name,  exists);
        *edge_count = *edge_count + 1;
    }
    
    return exists;
}


// Edge* edge_insert(int n1, int n2, int n3, int sn, Element *in_tet, Edge **edge_list, int *edge_count){
//     char id[6][100];
    
//     get_id(id[0], n1 , n2 , n3);
//     get_id(id[1], n3 , n2 , n1);
    
//     get_id(id[2], n3 , n1 , n2);
//     get_id(id[3], n2 , n1 , n3);
    
//     get_id(id[4], n2 , n3 , n1);
//     get_id(id[5], n1 , n3 , n2);


//     Edge *exists = NULL;
//     for(int i = 0; i < 6; i++){
//         HASH_FIND_STR( edge_hash, id[i], exists);
//         if(exists){
//             if(exists->boundary){
//                 exists->le = in_tet;
//             }
//             else
//                 exists->re = in_tet;
//             break;
//         }
//     }
//     if(!exists){
//         exists = (Edge *) malloc(sizeof(Edge));
//         sprintf(exists->name, "%i %i %i", n1, n2, n3);
//         exists->nodes[0] = n1;
//         exists->nodes[1] = n2;
//         exists->nodes[2] = n3;
//         exists->le = in_tet;
//         exists->re = NULL;
//         exists->boundary = 0;
//         exists->color = 0;
//         edge_list[*edge_count] = exists;
//         HASH_ADD_STR( edge_hash, name,  exists);
//         *edge_count = *edge_count + 1;
//     }
    
//     return exists;
// }


void populate_hash(int *color_hash, Edge *p_edge){
    color_hash[0] = 0; color_hash[1] = 0; color_hash[2] = 0; color_hash[3] = 0; color_hash[4] = 0;
    Element *re = p_edge->re;
    color_hash[p_edge->adj[0]->color]++;
    color_hash[p_edge->adj[1]->color]++;
    color_hash[p_edge->adj[2]->color]++;
    if(re){
        color_hash[p_edge->adj[3]->color]++;
        color_hash[p_edge->adj[4]->color]++;
        color_hash[p_edge->adj[5]->color]++;
    }
}

void find(Edge *p_edge, int *d1, int* d2, int *d3, int* d4){
    int color_list[] = {0,0,0,0,0,0};
    Element *re = p_edge->re;
    color_list[0] = p_edge->adj[0]->color;
    color_list[1] = p_edge->adj[1]->color;
    color_list[2] = p_edge->adj[2]->color;
    if(re){
        color_list[3]= p_edge->adj[3]->color;
        color_list[4]= p_edge->adj[4]->color;
        color_list[5]= p_edge->adj[5]->color;
    }
    
    *d1 = -1; *d2 = -1; *d3 = -1; *d4 = -1;
//    printf("colorlist %i %i %i %i %i %i\n", color_list[0], color_list[1], color_list[2], color_list[3], color_list[4], color_list[5]);
    for(int i = 0; i < 6; i++){
        for(int j = i; j < 6; j++){
            if( (color_list[i] == color_list[j]) && (color_list[i]!=0) &&(color_list[j]!=0) && (i!=j)){
                if(*d1 == -1){
                    *d1 = i; *d2 = j;
                }
                else{
                    *d3 = i; *d4 = j;
                    break;
                }
            }
        }
    }
    
}

int resolve(Edge *p_edge, int color_hash[5]){
    int pos = 1;
    while(color_hash[pos] > 0 && pos < 5)
        pos++;
    
    if(1 <= pos && pos <=4)
        return pos;
    else
        return 0;
}

Edge* swap_with(Edge *p_edge, int conflict_ID, int d1, int d2, int d3, int d4){
    int swap_list[] = {1,1,1,0,0,0};
    int swap_ID[] = {-1,-1,-1,-1,-1,-1};
    int swap_count = 0;
    Element *re = p_edge->re;
    
    if(re){
        swap_list[3] = 1;
        swap_list[4] = 1;
        swap_list[5] = 1;
    }
    
    if(d1 > -1 && d2 > -1){
        swap_list[d1] = 0;
        swap_list[d2] = 0;
    }
    if(d3 > -1 && d4 > -1){
        swap_list[d3] = 0;
        swap_list[d4] = 0;
    }
    
    for(int i = 0; i < 6; i++){
        if(swap_list[i] && p_edge->adj[i]->visited != conflict_ID && p_edge->adj[i]->color != 0){
            swap_ID[swap_count] = i;
            swap_count++;
        }
    }
    
    if(swap_count>0){
        return p_edge->adj[swap_ID[rand()%swap_count]];
    }
    else
        return NULL;
}


int main(int argc, char * argv[]) {
    time_t t;
    srand((unsigned) time(&t));
    size_t start, end;
    int node_count=0, tet_count=0, boundary_count=0, edge_count=0, size;
    char line[100];
    FILE * inFileP = fopen(argv[1], "r");
    fgets(line, sizeof line, inFileP);
    while(strncmp(line,"$Nodes", 6)!=0)
        fgets(line, sizeof line, inFileP);
    fscanf(inFileP, "%i", &node_count);
    
    printf("node_count is %i\n", node_count);
    Node *node_list = (Node *) malloc( node_count * sizeof(Node));
    
    
    for(int n = 0; n < node_count; n++){
        fscanf(inFileP, "%*i %lf %lf %lf", node_list[n].coords + 0, node_list[n].coords + 1, node_list[n].coords + 2);
        node_list[n].boundary = 0;
    }
    
    fgets(line, sizeof line, inFileP);
    while(strncmp(line,"$Elements", 8)!=0)
        fgets(line, sizeof line, inFileP);
    fgets(line, sizeof line, inFileP) ;
    sscanf(line, "%i", &size);
    
    
    Edge **boundary_list = (Edge **) malloc( size * sizeof(Edge*));
    Edge **edge_list = (Edge **) malloc( 4*size * sizeof(Edge*));
    Element **tet_list = (Element **) malloc( size * sizeof(Element*));
    int type;
    
    Edge *temp;
    Edge *exists;
    
    int a,b,c;
    
    printf("finding faces.\n");
    start = clock();
    for(int n = 0; n < size; n++){
        type = -1;
        fgets(line, sizeof line, inFileP) ;
        sscanf(line, "%*i %i", &type);
        if(type == 2){
            
            temp = (Edge *) malloc(sizeof(Edge));
            sscanf(line, "%*i %*i %*i %*i %*i %i %i %i", temp->nodes + 0, temp->nodes + 1, temp->nodes + 2);
            //            printf("%i %i %i\n", temp->nodes[0], temp->nodes[1], temp->nodes[2]);
            temp->nodes[0]--;
            temp->nodes[1]--;
            temp->nodes[2]--;
            temp->le = NULL;
            temp->re = NULL;
            temp->boundary = 1;
            temp->color = 0;
            
            node_list[temp->nodes[0]].boundary = 1;
            node_list[temp->nodes[1]].boundary = 1;
            node_list[temp->nodes[2]].boundary = 1;
            a = temp->nodes[0];
            b = temp->nodes[1];
            c = temp->nodes[2];

            if (a < b) {
               if (c < a) swap(&a,&c);
            } else {
              if (b < c) swap(&a,&b);
              else swap(&a,&c);
            } 
            if(c<b) swap(&b,&c);

            sprintf(temp->name, "%i %i %i", a,b,c);
            HASH_FIND_STR( edge_hash, temp->name, exists);
            
            if(!exists){
                HASH_ADD_STR( edge_hash, name,  temp);
                boundary_list[boundary_count] = temp;
                edge_list[edge_count] = temp;
                edge_count++;
                boundary_count++;
            }
            else
                free(temp);
            
        }
        else if(type == 4){
            tet_list[tet_count] = (Element *) malloc(sizeof(Element));
            sscanf(line, "%*i %*i %*i %*i %*i %i %i %i %i", tet_list[tet_count]->nodes + 0, tet_list[tet_count]->nodes + 1, tet_list[tet_count]->nodes + 2, tet_list[tet_count]->nodes + 3);
            tet_list[tet_count]->type = 2;
            tet_list[tet_count]->nodes[0]--;
            tet_list[tet_count]->nodes[1]--;
            tet_list[tet_count]->nodes[2]--;
            tet_list[tet_count]->nodes[3]--;
            tet_count++;
        }
        else{
            // printf("unrecognized %i\n", type);
        }
    }
    
    int n1, n2, n3;
    //initialize faces
    for(int tet = 0; tet < tet_count; tet++){
        // add side 0
        n1 = tet_list[tet]->nodes[1];
        n2 = tet_list[tet]->nodes[2];
        n3 = tet_list[tet]->nodes[3];
        tet_list[tet]->sides[0] = edge_insert(n1, n2, n3, 0, tet_list[tet], edge_list, &edge_count);
        
        // add side 1
        n1 = tet_list[tet]->nodes[2];
        n2 = tet_list[tet]->nodes[0];
        n3 = tet_list[tet]->nodes[3];
        tet_list[tet]->sides[1] = edge_insert(n1, n2, n3, 1, tet_list[tet], edge_list, &edge_count);


        // add side 2
        n1 = tet_list[tet]->nodes[0];
        n2 = tet_list[tet]->nodes[1];
        n3 = tet_list[tet]->nodes[3];
        tet_list[tet]->sides[2] = edge_insert(n1, n2, n3, 2, tet_list[tet], edge_list, &edge_count);

        // add side 3
        n1 = tet_list[tet]->nodes[2];
        n2 = tet_list[tet]->nodes[1];
        n3 = tet_list[tet]->nodes[0];
        tet_list[tet]->sides[3] = edge_insert(n1, n2, n3, 3, tet_list[tet], edge_list, &edge_count);     

    }



    printf("element count %i\n", size);
    printf("boundary_count %i\n", boundary_count);
    printf("tet_count %i\n", tet_count);
    printf("edge_count %i\n", edge_count);
    printf("Found all the faces!\n");
    //found all the faces
    
    // initialize neighbors.
    Element *curr_elem;
    Edge *curr_edge;
    for(int i=0; i < tet_count; i++){
        curr_elem = tet_list[i];
        for(int s = 0; s < 4; s++) {
            curr_edge = curr_elem->sides[s];
            curr_elem->neighbors[s] = (curr_edge->re == curr_elem) ? curr_edge->le : curr_edge->re;
        }
    }
    end = clock();
    printf("\nseconds elapsed %lf\n", ((double)(end-start))/CLOCKS_PER_SEC);


    
    for (int i = 0; i < tet_count; i++)
        tet_list[i]->pos = i;
    for (int i = 0; i < edge_count; i++)
        edge_list[i]->pos = i;
    
    

    printf("Coloring!\n");
    start = clock();
    // build adjacency lists
    Edge *p_edge;
    Element *le, *re;
    for(int e = 0; e < edge_count; e++){
        p_edge = edge_list[e];
        le = p_edge->le;
        re = p_edge->re;

         p_edge->adj[0] = le->sides[0] == p_edge ? le->sides[3]: le->sides[0];
         p_edge->adj[1] = le->sides[1] == p_edge ? le->sides[3]: le->sides[1];
         p_edge->adj[2] = le->sides[2] == p_edge ? le->sides[3]: le->sides[2];

         if(re){
             p_edge->adj[3] = re->sides[0] == p_edge ? re->sides[3]: re->sides[0];
             p_edge->adj[4] = re->sides[1] == p_edge ? re->sides[3]: re->sides[1];
             p_edge->adj[5] = re->sides[2] == p_edge ? re->sides[3]: re->sides[2];
         }
         else{
             p_edge->adj[3] = NULL;
             p_edge->adj[4] = NULL;
             p_edge->adj[5] = NULL;
         }
    }


    Edge **conflict_list = (Edge **) malloc(edge_count * sizeof(Edge*));

    // initial greedy coloring
    int color_hash[] = {0,0,0,0,0};
    int pos;
    int conflict_size = 0;
    for(int e = 0; e < edge_count; e++){
        conflict_list[conflict_size] = NULL;

        p_edge = edge_list[e];
        populate_hash(color_hash, p_edge);

        pos = -1;
        for(int i = 1; i <= 4; i++){
            if(color_hash[i] == 0 ){
                pos = i;
                break;
            }
        }

        if(1 <= pos && pos <=4)
            p_edge->color=pos;
        else{
            p_edge->color=0;
            conflict_list[conflict_size] = p_edge;
            conflict_size++;
        }
        edge_list[e]->visited = -1;
    }


    printf("There are %i conflicts\n", conflict_size);
    Edge *curr_conflict;
    int r_color, d1, d2, d3, d4;
    
    Edge *swap;
    int conflict_ID = 0;
    int iteration = 0;
    // conflict resolution
    while(conflict_size > 0){
        curr_conflict = conflict_list[conflict_size-1];
        curr_conflict->visited = conflict_ID;
        
        populate_hash(color_hash, curr_conflict);
        r_color = resolve(curr_conflict, color_hash);
        if(r_color){ // can it be resolved?
            curr_conflict->color = r_color;
            conflict_size--;
            conflict_ID++;
            continue;
        }
        
        find(curr_conflict, &d1, &d2, &d3, &d4);
        swap = swap_with(curr_conflict, conflict_ID, d1, d2, d3, d4);
        if(swap){ // swap
            curr_conflict->color = swap->color;
            swap->color = 0;
            conflict_list[conflict_size-1] = swap;
            continue;
        }
        
        
        if(d1 < 0 || d2 < 0 || d3 < 0 || d4 < 0){
            // cannot double
            conflict_ID++;
            continue;
        }
        
        // double
        curr_conflict->color= curr_conflict->adj[d1]->color;
        curr_conflict->adj[d1]->color = 0;
        curr_conflict->adj[d2]->color = 0;
        conflict_list[conflict_size-1] = curr_conflict->adj[d1];
        conflict_list[conflict_size  ] = curr_conflict->adj[d2];
        conflict_size++;
        conflict_ID++;
        
        
        
        // if(iteration%100 ==0)
        //     printf("\r(%i) conflicts left.", conflict_size);
        iteration++;
    }
    
    printf("\nDone coloring. %i\n", conflict_size);
    
    end = clock();

    printf("\nseconds elapsed %lf\n", ((double)(end-start))/CLOCKS_PER_SEC);


    color_hash[0] = 0; color_hash[1] = 0; color_hash[2] = 0; color_hash[3] = 0; color_hash[4] = 0;
    for(int e = 0; e < edge_count; e++){
        color_hash[edge_list[e]->color]++;
    }
    for(int i = 0; i < 5; i++)
        printf("color %i: %i\n", i, color_hash[i]);
    
    for(int elem=0; elem < tet_count; elem++){
        color_hash[0] = 0; color_hash[1] = 0; color_hash[2] = 0; color_hash[3] = 0; color_hash[4] = 0;
        color_hash[tet_list[elem]->sides[0]->color]++;
        color_hash[tet_list[elem]->sides[1]->color]++;
        color_hash[tet_list[elem]->sides[2]->color]++;
        color_hash[tet_list[elem]->sides[3]->color]++;
        if(color_hash[1] != 1 || color_hash[2] != 1 || color_hash[3] != 1 || color_hash[4] != 1 ){
            printf("problem %i %i %i %i %i\n", color_hash[0], color_hash[1], color_hash[2], color_hash[3], color_hash[4]);
            printf("problem s1 %i s2 %i s3 %i s4 %i\n", tet_list[elem]->sides[0]->color, tet_list[elem]->sides[1]->color, tet_list[elem]->sides[2]->color, tet_list[elem]->sides[2]->color);
            exit(-1);
        }
    }





    printf("writing cmsh file %s\n", argv[2]);
    FILE *outFile = fopen(argv[2], "w");
    color_hash[0] = 0; color_hash[1] = 0; color_hash[2] = 0; color_hash[3] = 0; color_hash[4] = 0;
    for(int e = 0; e < edge_count; e++){
        color_hash[edge_list[e]->color]++;
    }
   fprintf(outFile, "%i\n", node_count);
   for (int i = 0; i < node_count; i++)
       fprintf(outFile, "%.015lf %.015lf %.015lf\n",node_list[i].coords[0]
                                                   ,node_list[i].coords[1]
                                                   ,node_list[i].coords[2]);
   fprintf(outFile, "%i\n", tet_count);
   for (int i = 0; i < tet_count; i++)
       fprintf(outFile, "%i %i %i %i %i %i %i %i\n",tet_list[i]->nodes[0],
                                                    tet_list[i]->nodes[1],
                                                    tet_list[i]->nodes[2],
                                                    tet_list[i]->nodes[3],
                                                    tet_list[i]->sides[0]->pos, 
                                                    tet_list[i]->sides[1]->pos, 
                                                    tet_list[i]->sides[2]->pos, 
                                                    tet_list[i]->sides[3]->pos);

   fprintf(outFile, "%i\n", edge_count);
   for (int i = 0; i < edge_count; i++){
    if( edge_list[i]->re)
        fprintf(outFile, "%i %i %i %i %i %i\n",
                                             edge_list[i]->nodes[0],
                                             edge_list[i]->nodes[1],
                                             edge_list[i]->nodes[2],
                                             edge_list[i]->le->pos, 
                                             edge_list[i]->re->pos, 
                                             edge_list[i]->color -1);
    else
        fprintf(outFile, "%i %i %i %i %i %i\n",
                                             edge_list[i]->nodes[0],
                                             edge_list[i]->nodes[1],
                                             edge_list[i]->nodes[2],
                                             edge_list[i]->le->pos, 
                                             -1, 
                                             edge_list[i]->color -1);
    }
    fclose(outFile);

    
}
